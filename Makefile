all: push build deploy

push:
	git push

build:
	rm -rf dist
	pnpm run build

deploy:
	rsync -rvzP dist/ root@partidopirata.com.ar:/srv/http/loom.partidopirata.com.ar
