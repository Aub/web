const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const ServiceWorkerWebpackPlugin = require('serviceworker-webpack-plugin')
const BrotliPlugin = require('brotli-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const DotenvPlugin = require('dotenv-webpack')
const webpack = require('webpack')

module.exports = function buildConfig(dev) {
  if (dev) console.log('development mode')
  return {
    ...((dev && {
      mode: 'development',
      devtool: 'cheap-module-eval-source-map',
    }) || { mode: 'production' }),
    devServer: {
      contentBase: './dist',
      hot: true,
      host: '0.0.0.0',
    },
    entry: './src/main.js',
    output: {
      filename: '[hash].js',
      path: path.resolve(__dirname, 'dist'),
    },
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [
            ...((dev && ['cache-loader']) || []),
            'vue-style-loader',
            'css-loader',
          ],
        },
        {
          test: /\.scss$/,
          use: [
            ...((dev && ['cache-loader']) || []),
            'vue-style-loader',
            'css-loader',
            'sass-loader',
          ],
        },
        {
          test: /\.sass$/,
          use: [
            ...((dev && ['cache-loader']) || []),
            'vue-style-loader',
            'css-loader',
            'sass-loader?indentedSyntax',
          ],
        },
        {
          test: /\.svg$/,
          oneOf: [
            {
              resourceQuery: /inline/,
              loader: 'vue-svg-loader',
            },
            {
              loader: 'file-loader',
              query: {
                name: 'assets/[name].[hash:8].[ext]',
              },
            },
          ],
        },
        {
          test: /\.js$/,
          use: [
            ...((dev && ['cache-loader']) || []),
            {
              loader: 'buble-loader',
              options: {
                objectAssign: 'Object.assign',
                transforms: {
                  dangerousForOf: true,
                  asyncAwait: false,
                },
              },
            },
          ],
          include: path.join(__dirname, 'src'),
        },
        {
          test: /\.vue$/,
          use: [...((dev && ['cache-loader']) || []), 'vue-loader'],
        },
        {
          test: /\.(png|jpg|gif|woff2|woff|otf|ttf|eot)$/,
          loader: 'file-loader',
          options: {
            name: '[name].[ext]?[hash]',
          },
        },
      ],
    },
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src/'),
      },
      extensions: ['.wasm', '.mjs', '.js', '.json', '.vue'],
    },
    plugins: [
      new ServiceWorkerWebpackPlugin({
        entry: path.join(__dirname, 'src/sjw.js'),
      }),
      new VueLoaderPlugin(),
      ...((dev && [new webpack.HotModuleReplacementPlugin()]) || [
        new CompressionWebpackPlugin(),
        new BrotliPlugin(),
      ]),
      new HtmlWebpackPlugin({
        title: 'miniloom',
        template: 'src/index.html',
      }),
      new CopyPlugin([{ from: 'public', to: '.' }]),
      new DotenvPlugin(),
    ],
  }
}
