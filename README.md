# loom-frontend

## Project setup
```
yarn
# or npm i, or pnpm i
```

### Compiles and hot-reloads for development
```
yarn serve
# or npm run serve, or pnpm run serve
```

### Compiles and minifies for production
```
yarn build
# or npm run build, or pnpm run build
```

